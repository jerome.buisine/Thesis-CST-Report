\section{L'approche décomposition SVD}

\subsection{Decription de la méthode}
La décomposition en valeurs singulières (SVD) est une méthode de factorisation de matrice. L'utiliser dans le cadre de la réduction d'informations pour le traitement de l'image pourrait être intéressant. La Fig \ref{fig:03_Research_01_svd_decomposition} propose un aperçu de la factorisation proposée par la méthode.

\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{03.Research/01.SVD/svd_decomposition.png}
	\caption[Décomposition en valeurs singulières]{Méthode de factorisation utilisée pour réduction de dimension de l'image : décomposition en valeurs singulières}
	\label{fig:03_Research_01_svd_decomposition}
\end{figure}


\vspace{2mm}
\noindent
La méthode propose donc une factorisation de la matrice de la manière suivante :

$$M = U \times \Sigma \times V^*$$

\vspace{2mm}
\noindent
C'est le vecteur de valeurs singulières ($\Sigma$) auquel nous nous sommes principalement intéressé. En effet, la méthode de décomposition appliquée à une image réduite au canal de luminance $L$ de la transformation L*a*b (voir annexe \ref{appendix:lab_transformation}) ou la transformation MSCN \autocite{DBLP:journals/tip/MittalMB12} (voir annexe \ref{appendix:mscn_transformation}) qui permet d'obtenir le vecteur de valeurs singulières $\Sigma$.

\vspace{2mm}
\noindent
Pour une image de $200 \times 200$ d'une zone nous obtenons donc un vecteur de valeurs singulières de taille $200$. C'est ce vecteur que nous allons traiter par la suite.


% Aperçu valeurs SVD (courbes)
\vspace{2mm}
\noindent
Pourquoi s'être intéressé à la décomposition SVD ? Nous avons pu observer que suivant le nombre d'échantillons utilisés pour générer une image d'un scène, le vecteur de valeurs singulières semblaient significativement bien distinguer le niveau de bruit jusqu'à l'image de référence (voir Fig. \ref{fig:03_Research_01_svd_vectors_on_images_appart1opt02}). La Fig. \ref{fig:03_Research_01_svd_vectors_on_images_appart1opt02_zone_3} propose également un aperçu des valeurs singulières mais ici sur une zone précise de la scène, la zone 3.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{03.Research/01.SVD/svd_vector_on_images_appart1opt02_lab.png}
	\caption[Aperçu des valeurs singulières sur la scène Appart1opt02]{Aperçu du vecteur de valeurs singulières des images de la scène Appart1opt02 à différents niveaux de bruits. Ici le vecteur possède 800 valeurs car la décomposition a été appliquée sur l'ensemble de l'image et non une zone spécifique. \`{A} noter que le vecteur est obtenu à partir du canal \textbf{L} de l'image et qu'il a été ici normalisé.}
	\label{fig:03_Research_01_svd_vectors_on_images_appart1opt02}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{03.Research/01.SVD/svd_vector_on_images_appart1opt02_lab_zone3.png}
	\caption[Aperçu des valeurs singulières de la zone 3 de la scène Appart1opt02]{Aperçu du vecteur de valeurs singulières des sous-images de la zone 3 de la scène Appart1opt02 à différents niveaux de bruits. Ici le vecteur possède 200 valeurs car la décomposition a été appliquée sur une zone spécifique. \`{A} noter que le vecteur est obtenu à partir du canal \textbf{L} de la sous-image et qu'il a été ici normalisé.}
	\label{fig:03_Research_01_svd_vectors_on_images_appart1opt02_zone_3}
\end{figure}

\subsection{Sélection des attributs}

Nous avons donc pour chaque image traitée, un vecteur de $200$ valeurs à traiter. Dans cette section nous allons présenter les différentes manières (transformations de l'image) d'obtenir ce vecteur ainsi que les différentes approches de sélection des composantes de celui-ci. 

\subsubsection{Liste des attributs extraits}
\label{section:03_Research_01_attributes_list_without_selection}

% Description de la liste des attributs extraits (modification de l'image)
La liste des transformations ci-dessous appliquées à l'image sont utilisées pour obtention du vecteur de valeurs singulières de $200$ valeurs via la décomposition SVD.

\begin{itemize}
	\item \textbf{lab}
: utilisation de la luminance de l'image.
	
	\item \textbf{mscn}
: utilisation de la matrice MSCN.
	
	\item \textbf{low\_bits\_2}
: utilisation uniquement des 2 bits de poids faibles de la matrice de luminance (un octet de représentation).
	
	\item \textbf{low\_bits\_3}
: utilisation uniquement des 3 bits de poids faibles de la matrice de luminance.
	
	\item \textbf{low\_bits\_4}
: utilisation uniquement des 4 bits de poids faibles de la matrice de luminance.
	
	\item \textbf{low\_bits\_5}
: utilisation uniquement des 5 bits de poids faibles de la matrice de luminance.
	
	\item \textbf{low\_bits\_6}
: utilisation uniquement des 6 bits de poids faibles de la matrice de luminance.
	
	\item \textbf{low\_bits\_4\_shifted\_2} : utilisation uniquement des 4 bits de centraux de la matrice de luminance.
	
	\item \textbf{ica\_diff}
: utilisation d'une matrice calculée à partir de la différence entre la matrice de luminance et une image reconstruite à partir des 50 composantes principales à partir de la méthode \enquote{Independent Component Analysis} (ICA). 
	
	\item \textbf{svd\_trunc\_diff}
: utilisation d'une matrice calculée à partir de la différence entre la matrice de luminance et une image reconstruite à partir des 30 composantes principales à partir de la méthode de décomposition SVD.
	
	\item \textbf{ipca\_diff}
: utilisation d'une matrice calculée à partir de la différence entre la matrice de luminance et une image reconstruite à partir des 20 composantes principales à partir de la méthode de \enquote{Incremental Principal Components Analysis} (IPCA).
	
	\item \textbf{svd\_reconstruct} : utilisation d'une image reconstruite à partir des 110 dernières composantes de la décomposition SVD (voir Fig. \ref{fig:03_Research_01_svd_reconstruction}).
\end{itemize}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_00100.png}
		\caption{Scène Appart1opt02 zone 9 avec 100 samples\\}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_00250.png}
		\caption{Scène Appart1opt02 zone 9 avec 250 samples\\}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_00400.png}
		\caption{Scène Appart1opt02 zone 9 avec 400 samples\\}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_00550.png}
		\caption{Scène Appart1opt02 zone 9 avec 550 samples\\}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_reconstruct_00100.png}
		\caption{Scène Appart1opt02 zone 9 avec 100 samples reconstruite\\}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_reconstruct_00250.png}
		\caption{Scène Appart1opt02 zone 9 avec 250 samples reconstruite\\}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_reconstruct_00400.png}
		\caption{Scène Appart1opt02 zone 9 avec 400 samples reconstruite\\}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.23\textwidth}
		\includegraphics[width=\textwidth]{03.Research/01.SVD/compression/svd/appartAopt_zone9_reconstruct_00550.png}
		\caption{Scène Appart1opt02 zone 9 avec 550 samples reconstruite\\}
	\end{subfigure}
	
	\caption{Aperçu de la reconstruction de la scène Appart1opt02 zone 9 à différents niveaux d'échantillons (samples) avec les 110 dernières composantes SVD}
	\label{fig:03_Research_01_svd_reconstruction}
\end{figure}

\subsubsection{Approche dîtes naïve de sélection d'attributs}

La première approche de sélection d'attributs consiste à choisir $n$ composantes du vecteur de valeurs singulières avec $n \in [4, 8, 16, 26, 32, 40]$ à une position $P$ avec $|P| = 5$ (voir Fig. \ref{fig:03_Research_01_position_selection}). Cette approche peut paraître naïve mais elle permet déjà d'essayer un certains nombres de paramètres.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{03.Research/01.SVD/position_selection.png}
	\caption[Première approche de sélection des composantes du vecteur]{Sélection d'attributs sur différentes zones du vecteur de valeurs singulières. La position est choisie comme telle, au début, au quart, au centre; au trois quarts et en fin de vecteur. Si l'on décide de prendre $n = 20$ composantes à partir de la position $P$ centrale, l'interval sélectionné sera $[90, 120[$}
	\label{fig:03_Research_01_position_selection}
\end{figure}

La Fig. \ref{fig:03_Research_01_schema_recapitulatif} permet un aperçu récapitulatif de la manière dont l'image est traitée pour la sélection d'attributs d'entrée au modèle d'apprentissage.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{03.Research/01.SVD/recap_scheme.png}
	\caption[Schéma récapitulatif du traitement de l'image]{Schéma récapitulatif de traitement de l'image dans l'objectif de sélectionner les meilleurs attributs pour entrée au modèle}
	\label{fig:03_Research_01_schema_recapitulatif}
\end{figure}

\subsubsection{Autres approches de sélection des attributs}

\'{E}tant donné l'approche naïve de sélection d'attributs et le fait que les valeurs des composantes d'une scène à une autre diffèrent, la capacité d'un modèle de classifier correctement une image peut être impactée. Les listes ci-dessous énumèrent les approches exploitées dans le cadre de la thèse.

\vspace{2mm}

Sur l'ensemble des données :
\begin{itemize}
	\item \textbf{highest\_corr\_sv :} récupération des composantes les plus corrélées entre-elles (score de corrélation) sur l'ensemble des données disponibles
	\item \textbf{lowest\_corr\_sv :} récupération des composantes les moins corrélées entre-elles (score de corrélation) sur l'ensemble des données disponibles	
\end{itemize}

\vspace{2mm}

Sur l'image seule :
\begin{itemize}
	\item \textbf{sv\_std\_filters :} génération de 4 images supplémentaires à l'image de niveau de luminance en entrée, à partir des filtes \enquote{wiener} (fenêtres kernel [3, 3] et [5, 5]) et \enquote{median} (fenêtres kernel [3, 3] et [5, 5]).
	
	\item \textbf{wave\_sv\_std\_filters :} génération de 5 images supplémentaires à l'image de niveau de luminance en entrée, à partir des filtes \enquote{wiener} (fenêtres kernel [3, 3] et [5, 5]), \enquote{median} (fenêtres kernel [3, 3] et [5, 5]) et \enquote{wavelet}.
	
	\item \textbf{sv\_std\_filters\_full :} génération de 13 images supplémentaires à l'image de niveau de luminance en entrée, à partir des filtes \enquote{wiener} (fenêtres kernel [3, 3] et [5, 5]), \enquote{median} (fenêtres kernel [3, 3] et [5, 5]), \enquote{mean} (2 kernels), \enquote{gaussian blur} (6 kernels) et \enquote{wavelet}.
\end{itemize}

\vspace{2mm}
La décomposition est ensuite appliquée à chacune des images disponibles dans la liste des images. Ce qui donne une matrice de taille $l \times 200 \times 200$ avec $l$ le nombre d'images dans la liste. Pour chacune des composantes, les valeurs qui lui sont trouvées ($l$ valeurs) sont normalisées suivant cette composante et la valeur de l'écart-type est calculée. Les $n$ plus grandes (highest) ou petites (lowest) valeurs d'écart-type permettent de cibler les $n$ composantes à sélectionner (sélection dynamique pour l'image).

\vspace{2mm}

Sur l'image seule :
\begin{itemize}
	\item \textbf{sv\_entropy\_std\_filters :} les même filtres que \enquote{sv\_std\_filters\_full} soit, 13 images générées supplémentaires sont utilisées. La contribution à l'entropie de chaque composante du vecteur de valeur singulières est calculée. C'est ensuite la valeur de l'écart-type de ces contributions d'entropie qui sont utilisées comme valeurs de sélection des composantes.
\end{itemize}

\vspace{2mm}
Pourquoi s'intéresser à un traitement d'image seul pour sélection des attributs ? \'{E}tant donné les résultats sur une approche de sélection d'interval, il semblerait que la sélection des attributs soient fortement liée à la scène elle-même (structure de la scène). Une approche proposant dynamiquement de choisir les composantes pour une scène (ici l'image directement) serait plus judicieuse et permettrait d'avoir une méthode générique qu'importe les images de nouvelles scènes à traiter. Une étude plus approfondie permettrait de mettre en avant pour chaque scène les composantes les plus utilisées (répartition/distribution de sélection des composantes).


\subsection{Paramètres et résultats}

Avant de mettre en avant les résultats issus de la décomposition SVD, il est important de rappeler l'architecture des modèles utilisés, les manières de normaliser les données ainsi que le récapitulatif de l'ensemble des paramètres. 

\subsubsection{Modèles}

L'annexe \ref{appendix:models_architecture} présente ces différents modèles. En voici la liste :

\begin{itemize}
	\item \textbf{M1 :} Support Vector Machine
	\item \textbf{M2 :} Ensemble model (3 sub-models)
	\item \textbf{M3 :} Ensemble model v2 (5 sub-models)
\end{itemize}

\vspace{2mm}

Ces ensemble de modèles sont des  \enquote{voting classifier} avec le principe de
le vote équitable et réglementé sur les élections \enquote{douces}.


\subsubsection{Normalisation des données}

Au niveau du traitement des données, nous avons opter sur trois manières de fournir les données en entrée aux modèles :

\begin{itemize}
	\item \textbf{svd :} sans normalisation
	\item \textbf{svdn :} le sous-vector est normalisé avec ces propres valeurs
	\item \textbf{svdne :} le sous-vector est normalisé en utilisant les valeurs minimale et maximale
des sous-vectors ou composantes de l'ensemble du dataset.
\end{itemize}

\subsubsection{Résultats}

Les 5 meilleurs modèles et leurs résultats issus des attributs extraits avec une sélection dites naïves sont présentés dans le tableau \ref{table:03_Research_01_best_models_naive_selection}. Les simulations des scènes \enquote{Appart1opt02} et \enquote{SdbDroite} obtenues du meilleur modèle sont disponibles (voir Fig. \ref{fig:03_Research_01_best_model_simulation_sceneA} et \ref{fig:03_Research_01_best_model_simulation_sceneH}).

\begin{table}[h!]
	\centering
	\begin{tabular}{|>{\scriptsize}l|>{\scriptsize}c|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|}
		\hline
		Model & feature & size & interval & zones & ROC Train & ROC Val & ROC Test\\
		\hline
		M3 & lab (svd) & 40 & [80, 120[ & 12 & 0.9418 & 0.9023 & 0.9219 \\
		M2 & lab (svd) & 32 & [84, 116[ & 4 & 0.9158 & 0.8724 & 0.9153 \\
		M2 & lab (svd) & 40 & [80, 120[ & 12 & 0.9629 & 0.9049 & 0.9145 \\
		M2 & lab (svdne) & 26 & [87, 113[ & 6 & 0.9337 & 0.8763 & 0.9089 \\
		M3 & low\_bits\_2 (svd) & 40 & [0, 40[ & 12 & 0.9567 & 0.8417 & 0.9081 \\
		\hline
	\end{tabular}
	\caption{5 meilleurs modèles avec sélection \enquote{naïve} sur le score ROC AUC}
	\label{table:03_Research_01_best_models_naive_selection}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{03.Research/01.SVD/simulations/Appart1opt02_simulation_curve.png}
	\caption[Simulation des prédictions sur chaque zone de la scène Appart1opt02 (A) durant le rendu]{Simulation des prédictions sur chaque zone de la scène Appart1opt02 (A) durant le rendu. Ici les zones apprises (zones avec un fond bleu) sont correctement apprises tout comme les zones non apprises où le seuil est pratiquement identiques.}
	\label{fig:03_Research_01_best_model_simulation_sceneA}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{03.Research/01.SVD/simulations/SdbDroite_simulation_curve.png}
	\caption[Simulation des prédictions sur chaque zone de la scène SbdDroite (H) durant le rendu]{Simulation des prédictions sur chaque zone de la scène SbdDroite (H) durant le rendu. Ici les zones apprises (zones avec un fond bleu) sont presque correctement apprises (voir zones 1, 5, 9 qui possèdent pas mal d'oscillations avant détection de non bruit). Par contre, 2 zones sur 4 non apprises possèdent aucun arrêt de la part du modèle (zones 4 et 11), tout comme la zone 7 qui n'est pas correctement prédite.}
	\label{fig:03_Research_01_best_model_simulation_sceneH}
\end{figure}


Les 5 meilleurs modèles et leurs résultats issus des attributs extraits avec une sélection dites \enquote{automatisés} sont présentés dans le tableau \ref{table:03_Research_01_best_models_corr_selection}. Les simulations des scènes \enquote{Appart1opt02} et \enquote{SdbDroite} obtenues du meilleur modèle sont disponibles (voir Fig. \ref{fig:03_Research_01_best_model_corr_simulation_sceneA} et \ref{fig:03_Research_01_best_model_corr_simulation_sceneH}).

\begin{table}[h!]
	\centering
	\begin{tabular}{|>{\scriptsize}l|>{\scriptsize}c|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|>{\scriptsize}r|}
		\hline
		Model & feature & size & correlation & kind & zones & ROC Train & ROC Val & ROC Test\\
		\hline
		M3 & filters (svdne) & 80 & lowest & sv entropy std & 12 & 0.9970 & 0.8348 & 0.8864 \\
		M1 & lab (svd) & 10 & highest & components & 8 & 0.8510 & 0.8563 & 0.8740 \\
		M1 & lab (svdne) & 20 & highest & labels & 12 & 0.8100 & 0.8729 & 0.8729 \\
		M2 & lab (svdne) & 40 & highest & components & 12 & 0.9681 & 0.8670 & 0.8709 \\
		M1 & lab (svdne) & 25 & highest & components & 12 & 0.9311 & 0.8596 & 0.8595 \\
		M2 & filters (svdne) & 80 & highest & sv std & 12 & 1 & 0.7644 & 0.8519 \\
		\hline
	\end{tabular}
	\caption{5 meilleurs modèles avec sélection \enquote{automatisé} sur le score ROC AUC}
	\label{table:03_Research_01_best_models_corr_selection}
\end{table}


% TODO : ajout nouvelles courbes simulation
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{03.Research/01.SVD/simulations/Appart1opt02_simulation_curve.png}
	\caption[Simulation des prédictions sur chaque zone de la scène Appart1opt02 (A) durant le rendu]{Simulation des prédictions sur chaque zone de la scène Appart1opt02 (A) durant le rendu.}
	\label{fig:03_Research_01_best_model_corr_simulation_sceneA}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{03.Research/01.SVD/simulations/SdbDroite_simulation_curve.png}
	\caption[Simulation des prédictions sur chaque zone de la scène SbdDroite (H) durant le rendu]{Simulation des prédictions sur chaque zone de la scène SbdDroite (H) durant le rendu.}
	\label{fig:03_Research_01_best_model_corr_simulation_sceneH}
\end{figure}

\subsubsection{Conclusion}

Par rapport à l'ensemble de ces résultats obtenus, il est difficile de dire si le modèle peut être assez généraliste sur l'ensemble des scènes ou sur de nouvelles scènes. En effet, il semblerait que l'approche d'apprentissage présente certaines limites et ne permet pas assez bien de généraliser (zones non apprises). Peut-être que la structure de la scène est trop renseignée dans les attributs fournis en entrée au modèle ce qui l'empêche de mesurer le bruit présent dans la scène. Cette raison pourrait expliquer le sur-apprentissage de certaines scènes (comme Appart1opt02). L'approche naïve de sélection des composants amène aussi à cette généralisation non parfaite puisque les composantes du vecteur de valeurs singulières ne porteront certainement pas la même information d'une scène à une autre.

A noter que des approches deep learning (sans convolutions) ont été esssayées pour vérifier si la méthode SVM n'était pas assez robuste à l'apprentissage. Malheureusement les résultats n'ont pas été meilleur que ceux sur les architectures de modèles proposées.

\vspace{2mm}
\noindent
\textbf{Ressources :} \href{https://github.com/prise-3d/Thesis-NoiseDetection-attributes}{Projet github} comprenant l'ensemble des développements réalisés dans cette section.